﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.RefreshToken
{
    public class RefreshTokenClass
    {
        public string refreshToken { get; set; }
        public DateTime expiredDate { get; set; }

        public RefreshTokenClass()
        {
            refreshToken = "";
            expiredDate = new DateTime();
        }

        public TypeValidationsRefreshToken ValidaRefreshToken(string refreshTokenRequest)
        {
            if (this.expiredDate < DateTime.Now)
            {
                return TypeValidationsRefreshToken.ExpiredToken;
            }
            else if (!this.refreshToken.Equals(refreshTokenRequest))
            {
                return TypeValidationsRefreshToken.InvalidToken;
            }

            return TypeValidationsRefreshToken.ValidToken;
        }
    }
}
