﻿using APIHSAdmin.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.DAO
{
    public static partial class PGData
    {
        public async static Task<List<IUser>> ListaUsuario()
        {
            string query = "SELECT idusuario, username, role from usuario";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            List<IUser> usuarios = new List<IUser>();
            try
            {
                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                while (leitor.Read())
                {
                    IUser user = new UsuarioAdmin();

                    user.IDUser = Convert.ToInt32(leitor["idusuario"]);
                    user.UserName = leitor["username"].ToString();
                    user.Role = leitor["role"].ToString();

                    usuarios.Add(user);
                }
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return usuarios;
        } 

        public async static Task<IUser> SelecionaUsuarioAdmin(int id)
        {
            string query = "SELECT * from usuario WHERE idusuario = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            IUser user = new UsuarioAdmin();

            try
            {
                command.Parameters.AddWithValue("@id", id);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                if (leitor.Read())
                {
                    user.IDUser = id;
                    user.UserName = leitor["username"].ToString();
                    user.Role = leitor["role"].ToString();
                    ((UsuarioAdmin)user).CPF = leitor["cpf"].ToString();
                    ((UsuarioAdmin)user).AlteraSenha = (bool)leitor["alterasenha"];
                }
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return user;
        }

        public async static Task InsereUsuarioAdmin(IUser user)
        {
            string query = "INSERT INTO usuario (username, senha, role, cpf, alterasenha) " +
                "VALUES (@username, @senha, @role, @cpf, @alterasenha);" +
                "select currval(\'\"usuario_idusuario_seq\"\');";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@username", user.UserName);
                command.Parameters.AddWithValue("@senha", user.Senha);
                command.Parameters.AddWithValue("@role", user.Role);
                command.Parameters.AddWithValue("@cpf", ((UsuarioAdmin)user).CPF);
                command.Parameters.AddWithValue("@alterasenha", ((UsuarioAdmin)user).AlteraSenha);

                cn.Open();

                user.IDUser = Convert.ToInt32(await command.ExecuteScalarAsync());
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task AlteraUsuarioAdmin(UsuarioAdmin user, int id)
        {
            string query = "UPDATE usuario SET username = @username, role = @role, alterasenha = @alterasenha WHERE idusuario = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@username", user.UserName);
                command.Parameters.AddWithValue("@alterasenha", user.AlteraSenha);
                command.Parameters.AddWithValue("@role", user.Role);
                command.Parameters.AddWithValue("@id", id);

                cn.Open();

                await command.ExecuteNonQueryAsync();

            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task AlteraSenhaUsuarioAdmin(string senha, int id)
        {
            string query = "UPDATE usuario SET senha = @senha WHERE idusuario = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@senha", senha);
                command.Parameters.AddWithValue("@id", id);

                cn.Open();

                await command.ExecuteNonQueryAsync();

            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task DeletaUsuario(int id)
        {
            string query = "DELETE FROM usuario WHERE idusuario = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@id", id);

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task<bool> Login(IUser user)
        {
            string query = "SELECT idusuario, role, alterasenha FROM usuario WHERE username = @user AND senha = @senha";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@user", user.UserName);
                command.Parameters.AddWithValue("@senha", user.Senha);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                if (leitor.Read())
                {
                    verificador = true;
                    user.IDUser = Convert.ToInt32(leitor["idusuario"]);
                    user.Role = leitor["role"].ToString();
                    ((UsuarioAdmin)user).AlteraSenha = (bool)leitor["alterasenha"];
                }
                
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }

        public async static Task<bool> VerificaUserName(string username)
        {
            string query = "SELECT idusuario FROM usuario where username = @username";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@username", username);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }
        public async static Task<bool> VerificaUserName(string username, int idusuario)
        {
            string query = "SELECT idusuario FROM usuario where username = @username and idusuario <> @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@id", idusuario);
                command.Parameters.AddWithValue("@username", username);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }
    }
}
