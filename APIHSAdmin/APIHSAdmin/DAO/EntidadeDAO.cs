﻿using APIHSAdmin.Model;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.DAO
{
    public static partial class PGData
    {
        public static async Task<List<IEntidade>> ListaEntidade()
        {
            string query = "SELECT identidade, nome, situacao from entidade";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            List<IEntidade> entidades = new List<IEntidade>();
            try
            {
                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                while (leitor.Read())
                {
                    IEntidade entidade = new Entidade();

                    entidade.IDEntidade = Convert.ToInt32(leitor["identidade"]);
                    entidade.Nome = leitor["nome"].ToString();
                    entidade.Situacao = Convert.ToInt32(leitor["situacao"]);

                    entidades.Add(entidade);
                }
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return entidades;
        }

        public static async Task<IEntidade> SelecionaEntidade(int id)
        {
            string query = "SELECT * from entidade WHERE identidade = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            IEntidade entidade = new Entidade();
            try
            {
                command.Parameters.AddWithValue("@id", id);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                if (leitor.Read())
                {
                    entidade.IDEntidade = Convert.ToInt32(leitor["identidade"]);
                    entidade.Nome = leitor["nome"].ToString();
                    entidade.Situacao = Convert.ToInt32(leitor["situacao"]);
                    entidade.QuantidadeEmpresas = Convert.ToInt32(leitor["quantidadeempresas"]);
                    entidade.QuantidadeUsuarios = Convert.ToInt32(leitor["quantidadeusuarios"]);
                    entidade.QuantidadeFuncionarios = Convert.ToInt32(leitor["quantidadefuncionarios"]);
                    entidade.Cnpj = leitor["cnpj"].ToString();
                    entidade.Email = leitor["email"].ToString();
                    entidade.InicioContrato = Convert.ToDateTime(leitor["iniciocontrato"]);
                    entidade.IDEntidadeAcesso = Convert.ToInt32(leitor["identidadeacesso"]);
                }
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return entidade;
        }

        public static async Task InsereEntidade(IEntidade entidade)
        {
            string query = "INSERT INTO entidade (nome, quantidadeempresas, quantidadeusuarios, quantidadefuncionarios, situacao, " +
                "cnpj, email, iniciocontrato) VALUES (@nome, @qtdeempresas, @qtdeusuarios, @qtdefuncionario, @situacao, " +
                "@cnpj, @email, @iniciocontrato); select currval(\'\"entidade_identidade_seq\"\');";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@nome", entidade.Nome);
                command.Parameters.AddWithValue("@qtdeempresas", entidade.QuantidadeEmpresas);
                command.Parameters.AddWithValue("@qtdeusuarios", entidade.QuantidadeUsuarios);
                command.Parameters.AddWithValue("@qtdefuncionario", entidade.QuantidadeFuncionarios);
                command.Parameters.AddWithValue("@situacao", entidade.Situacao);
                command.Parameters.AddWithValue("@cnpj", entidade.Cnpj);
                command.Parameters.AddWithValue("@email", entidade.Email);
                command.Parameters.AddWithValue("@iniciocontrato", entidade.InicioContrato);

                cn.Open();

                entidade.IDEntidade = Convert.ToInt32(await command.ExecuteScalarAsync());
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task InsereEntidadeBanco(IEntidade entidade, string nomeBanco)
        {
            string query = "INSERT INTO \"Entidade\" (\"Descricao\", \"QuantidadeEmpresas\", \"QuantidadeUsuarios\", " +
                "\"QuantidadeFuncionarios\", \"Situacao\", \"NomeBanco\", \"Codigo\", \"Email\") " +
                "VALUES (@descricao, @qtdeempresas, @qtdeusuarios, @qtdefuncionario, @situacao, " +
                "@nomebanco, @codigo, @email); select currval(\'\"Entidade_IDEntidade_seq\"\');";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_ENTIDADE);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@descricao", entidade.Nome);
                command.Parameters.AddWithValue("@qtdeempresas", entidade.QuantidadeEmpresas);
                command.Parameters.AddWithValue("@qtdeusuarios", entidade.QuantidadeUsuarios);
                command.Parameters.AddWithValue("@qtdefuncionario", entidade.QuantidadeFuncionarios);
                command.Parameters.AddWithValue("@situacao", entidade.Situacao);
                command.Parameters.AddWithValue("@nomebanco", nomeBanco);
                command.Parameters.AddWithValue("@codigo", entidade.IDEntidade.ToString() + "00");
                command.Parameters.AddWithValue("@email", entidade.Email);

                cn.Open();

                entidade.IDEntidadeAcesso = Convert.ToInt32(await command.ExecuteScalarAsync());
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }
        public static async Task InsereIDEntidadeAcesso(int idEntidade, int idAcesso)
        {
            string query = "UPDATE entidade SET identidadeacesso = @idacesso WHERE identidade = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@idacesso", idAcesso);
                command.Parameters.AddWithValue("@id", idEntidade);

                cn.Open();

                await command.ExecuteNonQueryAsync();

            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task AlteraEntidade(IEntidade entidade, int id)
        {
            string query = "UPDATE entidade SET nome = @nome, quantidadeempresas = @qtdeempresas, quantidadeusuarios = @qtdeusuarios, " +
                "quantidadefuncionarios = @qtdefuncionario, situacao = @situacao, cnpj = @cnpj, email = @email, " +
                "iniciocontrato = @iniciocontrato WHERE identidade = @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@id", id);
                command.Parameters.AddWithValue("@nome", entidade.Nome);
                command.Parameters.AddWithValue("@qtdeempresas", entidade.QuantidadeEmpresas);
                command.Parameters.AddWithValue("@qtdeusuarios", entidade.QuantidadeUsuarios);
                command.Parameters.AddWithValue("@qtdefuncionario", entidade.QuantidadeFuncionarios);
                command.Parameters.AddWithValue("@situacao", entidade.Situacao);
                command.Parameters.AddWithValue("@cnpj", entidade.Cnpj);
                command.Parameters.AddWithValue("@email", entidade.Email);
                command.Parameters.AddWithValue("@iniciocontrato", entidade.InicioContrato);

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task<bool> VerificaNomeEntidade(string nome)
        {
            string query = "SELECT identidade FROM entidade where nome = @nome";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@nome", nome);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }

        public static async Task<bool> VerificaNomeEntidade(string nome, int idEntidade)
        {
            string query = "SELECT identidade FROM entidade where nome = @nome AND identidade <> @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@id", idEntidade);
                command.Parameters.AddWithValue("@nome", nome);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }

        public static async Task<bool> VerificaCNPJ(string cnpj)
        {
            string query = "SELECT identidade FROM entidade where cnpj = @cnpj";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@cnpj", cnpj);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }

        public static async Task<bool> VerificaCNPJ(string cnpj, int idEntidade)
        {
            string query = "SELECT identidade FROM entidade where cnpj = @cnpj AND identidade <> @id";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;

            try
            {
                command.Parameters.AddWithValue("@id", idEntidade);
                command.Parameters.AddWithValue("@cnpj", cnpj);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }

    }
}
