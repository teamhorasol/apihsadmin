﻿using APIHSAdmin.RefreshToken;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.DAO
{
    public static partial class PGData
    {
        public static async Task InsereRefreshToken(string refreshToken, int idUsuario, int expirationMinutes)
        {
            string query = "INSERT INTO usuariotoken (refreshtoken, horaacesso, idusuario, expirationdate) " +
                "VALUES (@refreshtoken, @horaacesso, @idusuario, @expiration)";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@refreshtoken", refreshToken);
                command.Parameters.AddWithValue("@horaacesso", DateTime.Now.TimeOfDay);
                command.Parameters.AddWithValue("@idusuario", idUsuario);
                command.Parameters.AddWithValue("@expiration", DateTime.Now.AddMinutes(expirationMinutes));

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task<RefreshTokenClass> SelecionaRefreshToken(int idUsuario)
        {
            string query = "SELECT RefreshToken, ExpirationDate FROM usuariotoken WHERE IDUsuario = @idusuario";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            RefreshTokenClass refreshToken = new RefreshTokenClass();

            try
            {
                command.Parameters.AddWithValue("@idusuario", idUsuario);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                if (leitor.Read())
                {
                    refreshToken.refreshToken = leitor[0].ToString();
                    refreshToken.expiredDate = Convert.ToDateTime(leitor[1].ToString());
                }
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return refreshToken;
        }

        public async static Task UpdateRefreshToken(string refreshToken, int idUsuario, int expirationMinutes)
        {
            string query = "UPDATE usuariotoken SET RefreshToken = @refreshtoken, ExpirationDate = @data" +
                " WHERE idusuario = @idusuario";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@idusuario", idUsuario);
                command.Parameters.AddWithValue("@refreshtoken", refreshToken);
                command.Parameters.AddWithValue("@data", DateTime.Now.AddMinutes(expirationMinutes));

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public async static Task DeleteRefreshToken(int idUsuario)
        {
            string query = "DELETE FROM usuariotoken WHERE idusuario = @idusuario";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_HSADMIN);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@idusuario", idUsuario);

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }
    }
}
