﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.DAO
{
    public static partial class PGData
    {
        public static async Task CriaDB(string nomeBanco)
        {
            string query = QuerysDB.QUERY_CREATEDB.Replace("*", "\"" + nomeBanco + "\"");

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_SERVER);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch(PostgresException ex)
            {
                throw ex;
            }
            catch(NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaEmpresa(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_EMPRESA, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaColaborador(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_COLABORADOR, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaMarcacao(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_MARCACAO, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaMarcacaoWeb(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_MARCACAOWEB, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaUsuarioExterno(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_USUARIOEXTERNO, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaUsuarioGestor(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_USUARIOGESTOR, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task TabelaUsuarioGestorToken(string nomeBanco)
        {
            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(QuerysDB.QUERY_CREATE_TABLE_USUARIOGESTORTOKEN, cn);

            try
            {
                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task InsereUsuarioGestor(string nomeBanco, string CNPJ_CPF, string senha)
        {
            string query = "INSERT INTO \"UsuarioGestor\" (\"UserName\", \"Senha\", \"IDPermissao\", \"Role\") " +
                "VALUES (@username, @senha, @idpermissao, @role)";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@username", CNPJ_CPF);
                command.Parameters.AddWithValue("@senha", senha);
                //TODO: inserir Permissao 
                command.Parameters.AddWithValue("@idpermissao", 1);
                command.Parameters.AddWithValue("@role", "Admin");

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task DeleteUsuarioGestor(string nomeBanco, string userName)
        {
            string query = "DELETE FROM \"UsuarioGestor\" WHERE \"UserName\" = @username";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_NOVAENTIDADE + @"'" + nomeBanco + "'");
            NpgsqlCommand command = new NpgsqlCommand(query, cn);

            try
            {
                command.Parameters.AddWithValue("@username", userName);

                cn.Open();

                await command.ExecuteNonQueryAsync();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
        }

        public static async Task<string> SelecionaNomeBanco(int idEntidadeAcesso)
        {
            string query = "SELECT \"NomeBanco\" FROM \"Entidade\" WHERE \"IDEntidade\" = @identidade";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_ENTIDADE);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            string nomeBanco = "";
            try
            {
                command.Parameters.AddWithValue("@identidade", idEntidadeAcesso);

                cn.Open();

                leitor = await command.ExecuteReaderAsync();

                if (leitor.Read())
                {
                    nomeBanco = leitor["NomeBanco"].ToString();
                }
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }
            return nomeBanco;
        }

        public static bool VerificaBD(string nomeBanco)
        {
            string query = "SELECT datdba FROM pg_catalog.pg_database where datname = @dbname";

            NpgsqlConnection cn = new NpgsqlConnection(STRINGCONNECTION_SERVER);
            NpgsqlCommand command = new NpgsqlCommand(query, cn);
            NpgsqlDataReader leitor;

            bool verificador = false;
            try
            {
                command.Parameters.AddWithValue("@dbname", nomeBanco);

                cn.Open();

                leitor = command.ExecuteReader();

                verificador = leitor.Read();
            }
            catch (PostgresException ex)
            {
                throw ex;
            }
            catch (NpgsqlException ex)
            {
                throw ex;
            }
            finally
            {
                cn.Close();
            }

            return verificador;
        }
    }
}
