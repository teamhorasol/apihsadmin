﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace APIHSAdmin.DAO
{
    public static class QuerysDB
    {
        public const string QUERY_CREATEDB = "CREATE DATABASE * " +
                        "WITH OWNER = horadev " +
                        "ENCODING = 'UTF8' " +
                        "LC_COLLATE = 'en_US.UTF-8' " +
                        "LC_CTYPE = 'en_US.UTF-8' " +
                        //"TABLESPACE = " + Não é necessário setar o TABLESPACE para criar o banco de dados.
                        "CONNECTION LIMIT = -1;";

        public const string QUERY_CREATE_TABLE_COLABORADOR = "CREATE TABLE public.\"Colaborador\" (" +
            "\"IDColaborador\" serial NOT NULL," +
            "\"PIS\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"Matricula\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"Nome\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"CPF\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"CTPS\" character varying(20) COLLATE pg_catalog.\"default\"," +
            "\"Serie\" character varying(20) COLLATE pg_catalog.\"default\"," +
            "\"Funcao\" character varying(50) COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"IDEmpresa\" integer NOT NULL," +
            "\"Desligado\" boolean NOT NULL," +
            " CONSTRAINT \"PK_IDColaborador\" PRIMARY KEY (\"IDColaborador\")," +
            " CONSTRAINT \"FK_IDEmpresa_Colaborador\" FOREIGN KEY(\"IDEmpresa\")" +
            "    REFERENCES public.\"Empresa\" (\"IDEmpresa\") MATCH SIMPLE" +
            "    ON UPDATE CASCADE " +
            "    ON DELETE CASCADE" +
            ")" +
            "WITH(" +
            "    OIDS = FALSE" +
            ")" +
            "TABLESPACE pg_default;" +
            "ALTER TABLE public.\"Colaborador\"" +
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_EMPRESA = "CREATE TABLE public.\"Empresa\" (" +
            "\"IDEmpresa\" serial NOT NULL," +
            "\"NomeFantasia\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"RazaoSocial\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"CNPJ_CPF\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"CEI\" text COLLATE pg_catalog.\"default\"," +
            "\"InscricaoEstadual\" text COLLATE pg_catalog.\"default\"," +
            "\"Endereco\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"AtividadeEconomica\" character varying(50) COLLATE pg_catalog.\"default\" NOT NULL," +
            " CONSTRAINT \"PK_Empresa\" PRIMARY KEY (\"IDEmpresa\")" +
            ")" +
            "WITH(" +
            "OIDS = FALSE" +
            ")" +
            "TABLESPACE pg_default;" +
            "ALTER TABLE public.\"Empresa\"" +
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_MARCACAO = "CREATE TABLE public.\"Marcacao\"(" +
            "\"IDMarcacao\" serial NOT NULL," +
            "\"NumeroSerie\" character(17) COLLATE pg_catalog.\"default\"," +
            "\"PIS\" character(12) COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"NSR\" character(12) COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"DataMarcacao\" date NOT NULL," +
            "\"Hora\" time without time zone NOT NULL," +
            "\"Origem\" integer NOT NULL," +
            "\"Excluida\" integer NOT NULL," +
            "\"Processada\" boolean NOT NULL," +
            "\"IDEmpresa\" integer NOT NULL," +
            " CONSTRAINT \"Marcacao_pkey\" PRIMARY KEY (\"IDMarcacao\")," +
            " CONSTRAINT \"Marcacao_PIS_Hora_DataMarcacao_key\" UNIQUE(\"PIS\", \"Hora\", \"DataMarcacao\")," +
            " CONSTRAINT \"FK_IDEmpresa_Marcacao\" FOREIGN KEY(\"IDEmpresa\")" +
            "REFERENCES public.\"Empresa\" (\"IDEmpresa\") MATCH SIMPLE " +
                "ON UPDATE CASCADE " +
                "ON DELETE CASCADE" +
                    ")" +
                    "WITH(" +
                    "OIDS = FALSE)" +
            "TABLESPACE pg_default;" +
                "ALTER TABLE public.\"Marcacao\"" +
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_MARCACAOWEB = "CREATE TABLE public.\"MarcacaoWEB\"("+
            "\"IDMarcacaoWEB\" serial NOT NULL," +
            "\"IDMarcacao\" integer NOT NULL,"+
            "\"Latitude\" character varying(100) COLLATE pg_catalog.\"default\","+
            "\"Longitude\" character varying(100) COLLATE pg_catalog.\"default\","+
            "\"CaminhoImagem\" character varying(100) COLLATE pg_catalog.\"default\","+
            "\"HoraServidor\" timestamp without time zone,"+
            "\"HoraBanco\" timestamp without time zone,"+
            " CONSTRAINT \"MarcacaoWEB_pkey\" PRIMARY KEY (\"IDMarcacaoWEB\"),"+
            " CONSTRAINT \"MarcacaoWEB_IDMarcacao_fkey\" FOREIGN KEY(\"IDMarcacao\")"+
            "    REFERENCES public.\"Marcacao\" (\"IDMarcacao\") MATCH SIMPLE "+
            "    ON UPDATE CASCADE "+
            "    ON DELETE CASCADE"+
            ")"+
            "WITH("+
            "    OIDS = FALSE"+
            ")"+
            "TABLESPACE pg_default;"+
            "ALTER TABLE public.\"MarcacaoWEB\""+
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_USUARIOEXTERNO = "CREATE TABLE public.\"UsuarioExterno\"(" +
            "\"IDUsuarioExterno\" serial NOT NULL," +
            "\"UserName\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"Senha\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"IDColaborador\" integer NOT NULL," +
            "\"FotoMobile\" boolean," +
            "\"LocalizacaoMobile\" boolean," +
            "\"AlteraSenha\" boolean NOT NULL," +
            " CONSTRAINT \"PK_IDUSUARIO\" PRIMARY KEY(\"IDUsuarioExterno\")," +
            " CONSTRAINT \"FK_IDColaborador\" FOREIGN KEY(\"IDColaborador\")" +
                "REFERENCES public.\"Colaborador\" (\"IDColaborador\") MATCH SIMPLE " +
                "ON UPDATE CASCADE " +
                "ON DELETE CASCADE" +
            ")" +
            "WITH(" +
            "    OIDS = FALSE" +
            ")" +
            "TABLESPACE pg_default;" +
            "ALTER TABLE public.\"UsuarioExterno\"" +
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_USUARIOGESTOR = "CREATE TABLE public.\"UsuarioGestor\"(" +
            "\"IDUsuarioGestor\" serial NOT NULL," +
            "\"UserName\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"Senha\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"IDPermissao\" integer NOT NULL," +
            "\"Role\" character varying(20) COLLATE pg_catalog.\"default\"," +
            " CONSTRAINT \"PK_IDUsuarioGestor\" PRIMARY KEY (\"IDUsuarioGestor\")" +
            ")" +
            "WITH(" +
            "    OIDS = FALSE" +
            ")" +
            "TABLESPACE pg_default;" +
            "ALTER TABLE public.\"UsuarioGestor\"" +
            "OWNER to horadev;";

        public const string QUERY_CREATE_TABLE_USUARIOGESTORTOKEN = "CREATE TABLE public.\"UsuarioGestorToken\"(" +
            "\"IDUsuarioGestorToken\" serial NOT NULL," +
            "\"RefreshToken\" text COLLATE pg_catalog.\"default\" NOT NULL," +
            "\"HoraAcesso\" time without time zone NOT NULL," +
            "\"IDUsuarioGestor\" integer NOT NULL," +
            "\"ExpirationDate\" timestamp without time zone NOT NULL," +
            " CONSTRAINT \"PK_IDUsuarioGestorToken\" PRIMARY KEY(\"IDUsuarioGestorToken\")," +
            " CONSTRAINT \"FK_IDUsuarioGestor\" FOREIGN KEY(\"IDUsuarioGestor\")" +
            "    REFERENCES public.\"UsuarioGestor\" (\"IDUsuarioGestor\") MATCH SIMPLE " +
            "    ON UPDATE CASCADE " +
            "    ON DELETE CASCADE" +
            ")" +
            "WITH(" +
            "    OIDS = FALSE" +
            ")" +
            "TABLESPACE pg_default;" +
            "ALTER TABLE public.\"UsuarioGestorToken\"" +
            "OWNER to horadev;";
    }
}
