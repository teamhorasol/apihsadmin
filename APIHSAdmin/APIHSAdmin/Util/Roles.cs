﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Util
{
    public class Roles
    {
        public const string System = "System";
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
