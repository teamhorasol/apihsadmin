﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIHSAdmin.Model;
using APIHSAdmin.Services;
using APIHSAdmin.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace APIHSAdmin.Controllers
{
    [Route("api/v1/[controller]")]
    [EnableCors("HSAdminPolicy")]
    [Authorize]
    [ApiController]
    public class EntidadesController : ControllerBase
    {
        private readonly IEntidadeService entidadeService;
        private readonly IDBService dbService;

        public EntidadesController(IEntidadeService serviceEntidade, IDBService serviceDB)
        {
            entidadeService = serviceEntidade;
            dbService = serviceDB;
        }
        // GET: api/Entidades
        [HttpGet]
        [Authorize(Roles = Roles.System + "," + Roles.Admin + "," + Roles.User)]
        public async Task<ActionResult<string>> Get()
        {
            try
            {
                List<IEntidade> entidades = await entidadeService.ListaEntidades();

                string responseJson = JsonConvert.SerializeObject(entidades, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                return Ok(responseJson);
            }
            catch (PostgresException ex)
            {
                return BadRequest("Contate o administrador do sistema. PostgresException");
            }
            catch(NpgsqlException ex)
            {
                return BadRequest("Contate o administrador do sistema. NpgsqlException");
            }
        }

        // GET: api/Entidades/5
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.System + "," + Roles.Admin + "," + Roles.User)]
        public async Task<ActionResult<string>> Get(int id)
        {
            try
            {
                IEntidade entidade = await entidadeService.SelecionaEntidade(id);

                string responseJson = JsonConvert.SerializeObject(entidade, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                return Ok(responseJson);
            }
            catch (PostgresException ex)
            {
                return BadRequest("Contate o administrador do sistema. PostgresException");
            }
            catch (NpgsqlException ex)
            {
                return BadRequest("Contate o administrador do sistema. NpgsqlException");
            }
        }

        // POST: api/Entidades
        [HttpPost]
        [Authorize(Roles = Roles.System + "," + Roles.Admin)]
        public async Task<ActionResult<IEntidade>> Post([FromBody] JObject objetoRequest)
        {
            try
            {
                IEntidade entidade = new Entidade(objetoRequest);

                List<string> mensagensErro = await ((EntidadeService)entidadeService).VerificaEntidade(entidade);

                if (mensagensErro.Count == 0)
                {
                    await entidadeService.InsereEntidade(entidade);;

                    string nomeBanco = await dbService.GeraNomeBanco(entidade);

                    await dbService.CriaBancoEntidade(entidade, nomeBanco);

                    await entidadeService.InsereIDEntidadeAcesso(entidade.IDEntidade, entidade.IDEntidadeAcesso);

                    return CreatedAtAction(nameof(Post), new { id = entidade.IDEntidade }, entidade);
                }
                else
                {
                    return BadRequest(mensagensErro);
                }
            }
            catch (PostgresException ex)
            {
                return BadRequest("Contate o administrador do sistema. PostgresException");
            }
            catch (NpgsqlException ex)
            {
                return BadRequest("Contate o administrador do sistema. NpgsqlException");
            }
        }

        // PUT: api/Entidades/5
        [HttpPut("{id}")]
        [Authorize(Roles = Roles.System + "," + Roles.Admin)]
        public async Task<ActionResult<IEntidade>> Put(int id, [FromBody] JObject objetoRequest)
        {
            try
            {
                IEntidade entidade = new Entidade(objetoRequest);

                List<string> mensagensErro = await ((EntidadeService)entidadeService).VerificaEntidadeAlteracao(entidade, id);

                if (mensagensErro.Count == 0)
                {
                    bool recuperaUsuario = objetoRequest.Value<bool>("RecuperaUsuario");

                    if (recuperaUsuario)
                    {
                        await entidadeService.RecuperaUsuario(entidade);
                    }

                    await entidadeService.AlteraEntidade(entidade, id);

                    return CreatedAtAction(nameof(Put), new { id = entidade.IDEntidade }, entidade);
                }
                else
                {
                    return BadRequest(mensagensErro);
                }
            }
            catch (PostgresException ex)
            {
                return BadRequest("Contate o administrador do sistema. PostgresException");
            }
            catch (NpgsqlException ex)
            {
                return BadRequest("Contate o administrador do sistema. NpgsqlException");
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.System + "," + Roles.Admin)]
        public async Task<ActionResult> Delete(int id)
        {
            return Ok();
        }
    }
}
