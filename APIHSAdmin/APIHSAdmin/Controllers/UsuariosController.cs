﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using APIHSAdmin.Model;
using APIHSAdmin.Services;
using APIHSAdmin.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace APIHSAdmin.Controllers
{
    [Route("api/v1/[controller]")]
    [EnableCors("HSAdminPolicy")]
    [Authorize]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IUserService userService;
        public UsuariosController(IUserService serviceUser)
        {
            userService = serviceUser;
        }
        // GET: api/Usuarios
        [HttpGet]
        [Authorize(Roles = Roles.System)]
        public async Task<ActionResult<string>> Get()
        {
            try
            {
                List<IUser> usuarios = await userService.ListaUsuario();

                string responseJson = JsonConvert.SerializeObject(usuarios, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                return Ok(responseJson);
            }
            catch(PostgresException ex)
            {
                //TODO log
                return BadRequest("Problemas no banco de dados.");
            }
            catch(Exception ex)
            {
                return BadRequest("Contate o administrador.");
            }
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.System)]
        public async Task<ActionResult<string>> Get(int id)
        {
            try
            {
                IUser usuario = await userService.SelecionaUsuario(id);

                string responseJson = JsonConvert.SerializeObject(usuario, Formatting.Indented, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                return Ok(responseJson);
            }
            catch (PostgresException ex)
            {
                //TODO log
                return BadRequest("Problemas no banco de dados.");
            }
            catch (Exception ex)
            {
                return BadRequest("Contate o administrador.");
            }
        }

        // POST: api/Usuarios
        [HttpPost]
        [Authorize(Roles = Roles.System)]
        public async Task<ActionResult<IUser>> Post([FromBody] JObject objetoRequest)
        {
            try
            {
                IUser usuario = new UsuarioAdmin(objetoRequest);
                List<string> mensagensErro = await ((UserService)userService).VerificaUsuarioAdmin((UsuarioAdmin)usuario);

                if (mensagensErro.Count == 0)
                {
                    await userService.InsereUsuario(usuario);

                    return CreatedAtAction(nameof(Post), new { id = usuario.IDUser }, usuario);
                }
                else
                {
                    return BadRequest(mensagensErro);
                }
            }
            catch (PostgresException ex)
            {
                //TODO log
                return BadRequest("Problemas no banco de dados.");
            }
            catch (Exception ex)
            {
                return BadRequest("Contate o administrador.");
            }
        }

        // PUT: api/Usuarios/5
        [HttpPut("{id}")]
        [Authorize(Roles = Roles.System)]
        public async Task<ActionResult<IUser>> Put(int id, [FromBody] JObject objetoRequest)
        {
            try
            {
                IUser usuario = new UsuarioAdmin(objetoRequest);
                List<string> mensagensErro = await ((UserService)userService).VerificaUsuarioAdminAlteracao((UsuarioAdmin)usuario, id);

                if (mensagensErro.Count == 0)
                {
                    await userService.AlteraUsuario(usuario, id);

                    if (((UsuarioAdmin)usuario).AlteraSenha)
                    {
                        await userService.AlteraSenhaUsuario(usuario.Senha, id);
                    }

                    return CreatedAtAction(nameof(Post), new { id = usuario.IDUser }, usuario);
                }
                else
                {
                    return BadRequest(mensagensErro);
                }
            }
            catch (PostgresException ex)
            {
                //TODO log
                return BadRequest("Problemas no banco de dados.");
            }
            catch (Exception ex)
            {
                return BadRequest("Contate o administrador.");
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.System)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await userService.DeletaUsuario(id);

                return Ok();
            }
            catch (PostgresException ex)
            {
                //TODO log
                return BadRequest("Problemas no banco de dados.");
            }
            catch (Exception ex)
            {
                return BadRequest("Contate o administrador.");
            }
        }
    }
}
