﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using APIHSAdmin.Model;
using APIHSAdmin.RefreshToken;
using APIHSAdmin.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;

namespace APIHSAdmin.Controllers
{
    [Route("api/v1/[controller]")]
    [EnableCors("HSAdminPolicy")]
    [Authorize]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly AppSettings settings;
        private readonly ITokenService tokenService;
        private readonly IUserService userService;

        public AuthController(IOptions<AppSettings> optionsSettings, IUserService serviceUser)
        {
            settings = optionsSettings.Value;
            tokenService = new TokenService(settings);
            userService = serviceUser;
            //logger = loggerParam;
        }
        // GET: api/Auth
        [HttpPost]
        [Route("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] JObject objetoJson)
        {
            try
            {
                IUser usuario = new UsuarioAdmin(objetoJson);

                bool verificaLogin = await userService.Login(usuario);

                if (verificaLogin)
                {
                    string refreshToken = tokenService.GenerateRefreshToken();

                    //TODO Refresh token no banco de dados
                    await tokenService.DeleteRefreshToken(usuario.IDUser);

                    await tokenService.InsereRefreshToken(refreshToken, usuario.IDUser, settings.RefreshTokenExpiration);

                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.PrimarySid, usuario.IDUser.ToString()),
                        new Claim(ClaimTypes.Name, usuario.UserName),
                        new Claim(ClaimTypes.Role, ((UsuarioAdmin)usuario).Role)
                    };

                    string token = tokenService.GenerateAccessToken(claims);

                    JObject colaboradorToken = new JObject(
                        new JProperty("token", token),
                        new JProperty("refreshToken", refreshToken),
                        new JProperty("role", ((UsuarioAdmin)usuario).Role),
                        new JProperty("alteraSenha", ((UsuarioAdmin)usuario).AlteraSenha));

                    string responseJson = JsonConvert.SerializeObject(colaboradorToken, Formatting.Indented, new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                    return Ok(responseJson);
                }

                return Unauthorized();
            }
            catch (FormatException)
            {
                return BadRequest("Objeto não está no formato correto.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost]
        [Route("changesenha")]
        public async Task<ActionResult> AlteraSenha([FromBody] JObject objetoJson)
        {
            try
            {
                string senhaNova = objetoJson.Value<string>("senhaNova");

                var identity = HttpContext.User.Identity as ClaimsIdentity;
                Claim claimIDUsuario = identity.Claims.ElementAt(0);

                int idUsuario = Convert.ToInt32(claimIDUsuario.Value);

                string senha = userService.CriptoSenha(senhaNova);

                if(!senha.Equals(""))
                {
                    await userService.AlteraSenhaUsuario(senha, idUsuario);

                    return Ok();
                }
                else
                {
                    return BadRequest("Senha inválida");
                }
            }
            catch (FormatException)
            {
                return BadRequest("Objeto não está no formato correto.");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("refresh")]
        [AllowAnonymous]
        public async Task<ActionResult> RefreshToken([FromBody] JObject objetoJson)
        {
            try
            {
                string headerAuthorization = Request.Headers["Authorization"].ToString();

                if (headerAuthorization.Length > 8)
                {
                    string token = headerAuthorization.Substring(7);
                    string refreshToken = objetoJson.Value<string>("refreshToken");

                    ClaimsPrincipal principal = new TokenService(settings).GetPrincipalFromExpiredToken(token);

                    int idUsuario = Convert.ToInt32(principal.Claims.ElementAt(0).Value);

                    RefreshTokenClass savedRefreshToken = await tokenService.SelecionaRefreshToken(idUsuario);

                    switch (savedRefreshToken.ValidaRefreshToken(refreshToken))
                    {
                        //Atualiza o token e o refreshToken. Atualiza o refreshToken no banco.
                        case TypeValidationsRefreshToken.ValidToken:

                            string newRefreshToken = tokenService.GenerateRefreshToken();

                            await tokenService.UpdateRefreshToken(newRefreshToken, idUsuario, settings.RefreshTokenExpiration);

                            string newToken = tokenService.GenerateAccessToken(principal.Claims);

                            JObject objectToken = new JObject(new JProperty("token", newToken), new JProperty("refreshToken", newRefreshToken));

                            return Ok(JsonConvert.SerializeObject(objectToken));

                        case TypeValidationsRefreshToken.ExpiredToken:

                            await tokenService.DeleteRefreshToken(idUsuario);
                            return BadRequest("Sessão Expirada");

                        case TypeValidationsRefreshToken.InvalidToken:

                            await tokenService.DeleteRefreshToken(idUsuario);
                            return BadRequest("Sessão inválida");

                        default:
                            return BadRequest();
                    }
                }
                else
                {
                    return Unauthorized();
                }
            }
            catch (SecurityTokenInvalidSignatureException)
            {
                return Unauthorized();
            }
            catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
