﻿using APIHSAdmin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public interface IDBService
    {
        Task CriaBancoEntidade(IEntidade entidade, string nomeBanco);

        Task<string> GeraNomeBanco(IEntidade entidade);
    }
}
