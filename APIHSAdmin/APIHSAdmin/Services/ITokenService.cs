﻿using APIHSAdmin.RefreshToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public interface ITokenService
    {
        string GenerateAccessToken(IEnumerable<Claim> claims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
        Task DeleteRefreshToken(int idUsuario);
        Task InsereRefreshToken(string refreshToken, int idUsuario, int expirationMinutes);
        Task UpdateRefreshToken(string refreshToken, int idUsuario, int expirationMinutes);
        Task<RefreshTokenClass> SelecionaRefreshToken(int idUsuario);
    }
}
