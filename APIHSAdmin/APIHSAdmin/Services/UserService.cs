﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using APIHSAdmin.DAO;
using APIHSAdmin.Model;

namespace APIHSAdmin.Services
{
    public class UserService : IUserService
    {
        public async Task<List<string>> VerificaUsuarioAdmin(UsuarioAdmin user)
        {
            Task<bool> t1 = VerificaUserName(user.UserName);
            Task<bool> t2 = VerificaUserNameExistente(user.UserName);
            Task<bool> t3 = VerificaSenha(user.Senha);
            Task<bool> t4 = VerificaRole(user.Role);
            Task<bool> t5 = ValidaCpf(user.CPF);

            await Task.WhenAll(t1, t2, t3, t4, t5);

            List<string> mensagens = new List<string>();

            if (!t1.Result)
            {
                mensagens.Add("UserName inválido");
            }

            if (t2.Result)
            {
                mensagens.Add("UserName já existe");
            }

            if (!t3.Result)
            {
                mensagens.Add("Senha inválida");
            }

            if (!t4.Result)
            {
                mensagens.Add("Permissão inválida");
            }

            if (!t5.Result)
            {
                mensagens.Add("CPF inválido");
            }

            return mensagens;
        }

        public async Task<List<string>> VerificaUsuarioAdminAlteracao(UsuarioAdmin user, int idusuario)
        {
            Task<bool> t1 = VerificaUserName(user.UserName);
            Task<bool> t2 = VerificaUserNameExistente(user.UserName, idusuario);
            //Task<bool> t3 = VerificaSenha(user.Senha);
            Task<bool> t4 = VerificaRole(user.Role);
            Task<bool> t5 = ValidaCpf(user.CPF);

            await Task.WhenAll(t1, t2, t4, t5);

            List<string> mensagens = new List<string>();

            if (!t1.Result)
            {
                mensagens.Add("UserName inválido");
            }

            if (t2.Result)
            {
                mensagens.Add("UserName já existe");
            }

            //if (!t3.Result)
            //{
            //    mensagens.Add("Senha inválida");
            //}

            if (!t4.Result)
            {
                mensagens.Add("Permissão inválida");
            }

            if (!t5.Result)
            {
                mensagens.Add("CPF inválido");
            }

            return mensagens;
        }
        public async Task AlteraUsuario(IUser user, int id)
        {
            await PGData.AlteraUsuarioAdmin((UsuarioAdmin)user, id);
        }

        public async Task AlteraSenhaUsuario(string senha, int id)
        {
            await PGData.AlteraSenhaUsuarioAdmin(senha, id);
        }

        public string CriptoSenha(string senha)
        {
            if (senha != null)
            {
                if (!senha.Equals(""))
                {
                    try
                    {
                        byte[] salt = new byte[32];

                        salt = Convert.FromBase64String("31302928272625242322212019181716");

                        byte[] arrayBytesenha = new byte[32];

                        using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(senha, salt, 10000, HashAlgorithmName.SHA256))
                        {
                            arrayBytesenha = pbkdf2.GetBytes(32);
                        }

                        string senhaCripto = Convert.ToBase64String(arrayBytesenha);

                        return senhaCripto;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return senha;
                }
            }
            else
            {
                return "";
            }
        }

        public async Task DeletaUsuario(int id)
        {
            await PGData.DeletaUsuario(id);
        }

        public async Task InsereUsuario(IUser user)
        {
            await PGData.InsereUsuarioAdmin(user);
        }

        public async Task<List<IUser>> ListaUsuario()
        {
            List<IUser> usuarios = await PGData.ListaUsuario();

            return usuarios;
        }

        public async Task<bool> Login(IUser user)
        {
            bool autenticado = await PGData.Login(user);

            return autenticado;
        }

        public async Task<IUser> SelecionaUsuario(int id)
        {
            IUser usuario = await PGData.SelecionaUsuarioAdmin(id);

            return usuario;
        }

        private static async Task<bool> VerificaUserName(string user)
        {
            return await Task.Run(() =>
            {
                if (user == null || user.Length < 3)
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        private static async Task<bool> VerificaUserNameExistente(string user)
        {
            return await Task.Run(async () =>
            {
                bool verificador = await PGData.VerificaUserName(user);

                return verificador;
            });
        }

        private static async Task<bool> VerificaUserNameExistente(string user, int idUsuario)
        {
            return await Task.Run(async () =>
            {
                bool verificador = await PGData.VerificaUserName(user, idUsuario);

                return verificador;
            });
        }

        private static async Task<bool> VerificaSenha(string senha)
        {
            return await Task.Run(() =>
            {
                if (senha == null || senha.Length < 3)
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        private static async Task<bool> VerificaRole(string role)
        {
            string[] roles = new string[] { "Admin", "User", "System" };
            return await Task.Run(() =>
            {
                if (role == null || !roles.Contains(role))
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        public static async Task<bool> ValidaCpf(string cpf)
        {
            return await Task.Run(() =>
            {
                if(cpf == null)
                {
                    return Task.FromResult(false);
                }

                if (cpf.Length != 11)
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(VerificadorCPF(cpf));
            });
        }

        private static bool VerificadorSomenteNumero(string numero)
        {
            if(numero != null)
            {
                foreach (char caracter in numero)
                {
                    if (caracter < '0' || caracter > '9')
                        return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        private static bool VerificadorCPF(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            string tempCpf;
            string digito;
            int soma;
            int resto;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }

            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }

            resto = soma % 11;

            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }


            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }
    }
}
