﻿using APIHSAdmin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public interface IEntidadeService
    {
        Task<List<IEntidade>> ListaEntidades();
        Task<IEntidade> SelecionaEntidade(int id);
        Task InsereEntidade(IEntidade entidade);
        Task InsereEntidadeBanco(IEntidade entidade, string nomeBanco);
        Task InsereIDEntidadeAcesso(int idEntidade, int idAcesso);
        Task AlteraEntidade(IEntidade entidade, int id);
        Task DeletaEntidade(int id);
        Task RecuperaUsuario(IEntidade entidade);

    }
}
