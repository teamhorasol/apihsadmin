﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using APIHSAdmin.DAO;
using APIHSAdmin.Model;

namespace APIHSAdmin.Services
{

    public class EntidadeService : IEntidadeService
    {
        public async Task<List<string>> VerificaEntidade(IEntidade entidade)
        {
            Task<bool> t1 = VerificaNome(entidade.Nome);
            Task<bool> t2 = VerificaNomeExistente(entidade.Nome);
            Task<bool> t3 = VerificaQuantidade(entidade.QuantidadeEmpresas);
            Task<bool> t4 = VerificaQuantidade(entidade.QuantidadeUsuarios);
            Task<bool> t5 = VerificaQuantidade(entidade.QuantidadeFuncionarios);
            Task<bool> t6 = VerificaSituacao(entidade.Situacao);
            Task<bool> t7 = VerificaCNPJCPF(entidade.Cnpj);
            Task<bool> t8 = VerificaCNPJCPFExistente(entidade.Cnpj);
            Task<bool> t9 = VerificaEmail(entidade.Email);

            await Task.WhenAll(t1, t2, t3, t4, t5, t6, t7, t8, t9);

            List<string> mensagens = new List<string>();

            if (!t1.Result)
            {
                mensagens.Add("Nome inválido.");
            }
            if (t2.Result)
            {
                mensagens.Add("Nome já existe.");
            }
            if (!t3.Result)
            {
                mensagens.Add("Quantidade de empresas Inválida");
            }
            if (!t4.Result)
            {
                mensagens.Add("Quantidade de usuários Inválida");
            }
            if (!t5.Result)
            {
                mensagens.Add("Quantidade de funcionários Inválida");
            }
            if (!t6.Result)
            {
                mensagens.Add("Situação Inválida");
            }
            if (!t7.Result)
            {
                mensagens.Add("CNPJ/CPF Inválido");
            }
            if (t8.Result)
            {
                mensagens.Add("CNPJ/CPF já existe");
            }
            if (!t9.Result)
            {
                mensagens.Add("Email Inválido");
            }

            return mensagens;
        }

        public async Task<List<string>> VerificaEntidadeAlteracao(IEntidade entidade, int idEntidade)
        {
            Task<bool> t1 = VerificaNome(entidade.Nome);
            Task<bool> t2 = VerificaNomeExistente(entidade.Nome, idEntidade);
            Task<bool> t3 = VerificaQuantidade(entidade.QuantidadeEmpresas);
            Task<bool> t4 = VerificaQuantidade(entidade.QuantidadeUsuarios);
            Task<bool> t5 = VerificaQuantidade(entidade.QuantidadeFuncionarios);
            Task<bool> t6 = VerificaSituacao(entidade.Situacao);
            Task<bool> t7 = VerificaCNPJCPF(entidade.Cnpj);
            Task<bool> t8 = VerificaCNPJCPFExistente(entidade.Cnpj, idEntidade);
            Task<bool> t9 = VerificaEmail(entidade.Email);

            await Task.WhenAll(t1, t2, t3, t4, t5, t6, t7, t8, t9);

            List<string> mensagens = new List<string>();

            if (!t1.Result)
            {
                mensagens.Add("Nome inválido.");
            }
            if (t2.Result)
            {
                mensagens.Add("Nome já existe.");
            }
            if (!t3.Result)
            {
                mensagens.Add("Quantidade de empresas Inválida");
            }
            if (!t4.Result)
            {
                mensagens.Add("Quantidade de usuários Inválida");
            }
            if (!t5.Result)
            {
                mensagens.Add("Quantidade de funcionários Inválida");
            }
            if (!t6.Result)
            {
                mensagens.Add("Situação Inválida");
            }
            if (!t7.Result)
            {
                mensagens.Add("CNPJ/CPF Inválido");
            }
            if (t8.Result)
            {
                mensagens.Add("CNPJ/CPF já existe");
            }
            if (!t9.Result)
            {
                mensagens.Add("Email Inválido");
            }

            return mensagens;
        }

        public async Task<bool> VerificaNome(string nome)
        {
            return await Task.Run(() =>
            {
                if (nome == null || nome.Equals(""))
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        public async Task<bool> VerificaNomeExistente(string nome)
        {
            return await Task.Run(async () =>
           {
               bool verificador = await PGData.VerificaNomeEntidade(nome);

               return verificador;
           });
        }

        public async Task<bool> VerificaNomeExistente(string nome, int idEntidade)
        {
            return await Task.Run(async () =>
            {
                bool verificador = await PGData.VerificaNomeEntidade(nome, idEntidade);

                return verificador;
            });
        }

        public async Task<bool> VerificaQuantidade(int quantidade)
        {
            return await Task.Run(() =>
            {
                if (quantidade <= 0)
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        public async Task<bool> VerificaSituacao(int situacao)
        {
            return await Task.Run(() =>
            {
                if (situacao > 1 || situacao < 0)
                {
                    return Task.FromResult(false);
                }

                return Task.FromResult(true);
            });
        }

        public static async Task<bool> VerificaEmail(string email)
        {
            return await Task.Run(() =>
            {

                email = email.Trim();
                string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                    @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                    @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

                Regex exp = new Regex(strRegex);

                if (email.Length > 100 || email == null || !exp.IsMatch(email))
                {
                    return Task.FromResult(false);
                }
                else
                {
                    return Task.FromResult(true);
                }
            });
        }

        public static async Task<bool> VerificaCNPJCPF(string cnpj)
        {
            return await Task.Run(() =>
            {
                if (cnpj == null)
                {
                    return Task.FromResult(false);
                }

                foreach (char c in cnpj)
                {
                    if (c < '0' || c > '9')
                        return Task.FromResult(false);
                }

                bool verificador = false;

                if (cnpj.Length == 11)
                {
                    verificador = ValidaCPF(cnpj);

                    return Task.FromResult(verificador);
                }
                else if (cnpj.Length == 14)
                {
                    verificador = ValidaCNPJ(cnpj);

                    return Task.FromResult(verificador);
                }

                return Task.FromResult(false);
            });
        }

        public static async Task<bool> VerificaCNPJCPFExistente(string cnpj)
        {
            return await Task.Run(async () =>
            {
                bool verificador = await PGData.VerificaCNPJ(cnpj);

                return verificador;

            });
        }
        public static async Task<bool> VerificaCNPJCPFExistente(string cnpj, int idEntidade)
        {
            return await Task.Run(async () =>
            {
                bool verificador = await PGData.VerificaCNPJ(cnpj, idEntidade);

                return verificador;

            });
        }

        private static bool ValidaCNPJ(string cnpj)
        {
            int[] multiplicador1 = { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma = 0;
            int digito1;
            int digito2;

            for (int i = 0; i < 12; i++)
            {
                soma += multiplicador1[i] * Convert.ToInt16(cnpj.Substring(i, 1));
            }

            int resto = soma % 11;

            if (resto < 2)
                digito1 = 0;
            else
                digito1 = 11 - resto;
            soma = 0;

            for (int i = 0; i < 13; i++)
            {
                soma += multiplicador2[i] * Convert.ToInt16(cnpj.Substring(i, 1));
            }

            resto = soma % 11;

            if (resto < 2)
                digito2 = 0;
            else
                digito2 = 11 - resto;

            return (digito1.ToString() + digito2.ToString()).Equals(cnpj.Substring(12));
        }

        private static bool ValidaCPF(string cpf)
        {
            if (cpf == null || cpf.Length != 11)
            {
                return false;
            }

            foreach (char c in cpf)
            {
                if (c < '0' || c > '9')
                    return false;
            }

            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };

            string tempCpf;
            string digito;
            int soma;
            int resto;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            }

            resto = soma % 11;
            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }

            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;

            for (int i = 0; i < 10; i++)
            {
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            }

            resto = soma % 11;

            if (resto < 2)
            {
                resto = 0;
            }
            else
            {
                resto = 11 - resto;
            }


            digito = digito + resto.ToString();

            return cpf.EndsWith(digito);
        }

        public async Task AlteraEntidade(IEntidade entidade, int id)
        {
            await PGData.AlteraEntidade(entidade, id);
        }

        public Task DeletaEntidade(int id)
        {
            throw new NotImplementedException();
        }

        public async Task InsereEntidade(IEntidade entidade)
        {
            await PGData.InsereEntidade(entidade);
        }

        public async Task InsereEntidadeBanco(IEntidade entidade, string nomeBanco)
        {
            await PGData.InsereEntidadeBanco(entidade, nomeBanco);
        }

        public async Task<List<IEntidade>> ListaEntidades()
        {
            List<IEntidade> entidades = await PGData.ListaEntidade();

            return entidades;
        }

        public async Task<IEntidade> SelecionaEntidade(int id)
        {
            IEntidade entidade = await PGData.SelecionaEntidade(id);

            return entidade;
        }
        
        public async Task InsereIDEntidadeAcesso(int idEntidade, int idAcesso)
        {
            await PGData.InsereIDEntidadeAcesso(idEntidade, idAcesso);
        }

        public async Task RecuperaUsuario(IEntidade entidade)
        {
            string nomeBanco = await SelecionaNomeBanco(entidade.IDEntidadeAcesso);

            await DeletaUsuarioGestor(nomeBanco, entidade);

            await InsereUsuarioGestor(nomeBanco, entidade);
        }

        private async Task<string> SelecionaNomeBanco(int idEntidadeAcesso)
        {
            string nomeBanco = await PGData.SelecionaNomeBanco(idEntidadeAcesso);

            return nomeBanco;
        }

        private async Task DeletaUsuarioGestor(string nomeBanco, IEntidade entidade)
        {
            await PGData.DeleteUsuarioGestor(nomeBanco, entidade.Cnpj);
        }

        private async Task InsereUsuarioGestor(string nomeBanco, IEntidade entidade)
        {
            string senha = CriptoSenha(entidade.Cnpj);
            await PGData.InsereUsuarioGestor(nomeBanco, entidade.Cnpj, senha);
        }

        private string CriptoSenha(string Senha)
        {
            byte[] salt = new byte[32];

            salt = Convert.FromBase64String("31302928272625242322212019181716");

            byte[] arrayBytesenha = new byte[32];

            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(Senha, salt, 10000, HashAlgorithmName.SHA256))
            {
                arrayBytesenha = pbkdf2.GetBytes(32);
            }

            string senhaCripto = Convert.ToBase64String(arrayBytesenha);

            return senhaCripto;
        }
    }
}
