﻿using APIHSAdmin.DAO;
using APIHSAdmin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public class DBService: IDBService
    {
        public async Task CriaBancoEntidade(IEntidade entidade, string nomeBanco)
        {
            try
            {
                //TODO: Verificar o nome do banco
                bool verificador = PGData.VerificaBD(nomeBanco);

                if (!verificador)
                {
                    await PGData.CriaDB(nomeBanco);
                    await PGData.TabelaEmpresa(nomeBanco);
                    await PGData.TabelaColaborador(nomeBanco);
                    await PGData.TabelaMarcacao(nomeBanco);
                    await PGData.TabelaMarcacaoWeb(nomeBanco);
                    await PGData.TabelaUsuarioExterno(nomeBanco);
                    await PGData.TabelaUsuarioGestor(nomeBanco);
                    await PGData.TabelaUsuarioGestorToken(nomeBanco);

                    //Insere os dados da entidade no banco Entidades
                    await PGData.InsereEntidadeBanco(entidade, nomeBanco);

                    //Inserir usuário para cadastrar todas as coisas.
                    string senha = CriptoSenha(entidade.Cnpj);

                    await PGData.InsereUsuarioGestor(nomeBanco, entidade.Cnpj, senha);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> GeraNomeBanco(IEntidade entidade)
        {
            return await Task.Run(() => 
            {
                StringBuilder nomeBanco = new StringBuilder();
                int indexFinalNome = entidade.Nome.IndexOf(" ");

                string primeiroNome = "";

                if(indexFinalNome == -1)
                {
                    primeiroNome = entidade.Nome;
                }
                else
                {
                    primeiroNome = entidade.Nome.Substring(0, indexFinalNome);
                }

                nomeBanco.Append(primeiroNome);
                nomeBanco.Append("_");
                nomeBanco.Append(entidade.IDEntidade.ToString());

                return nomeBanco.ToString();
            });

        }

        public string CriptoSenha(string Senha)
        {
            byte[] salt = new byte[32];

            salt = Convert.FromBase64String("31302928272625242322212019181716");

            byte[] arrayBytesenha = new byte[32];

            using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(Senha, salt, 10000, HashAlgorithmName.SHA256))
            {
                arrayBytesenha = pbkdf2.GetBytes(32);
            }

            string senhaCripto = Convert.ToBase64String(arrayBytesenha);

            return senhaCripto;
        }
    }
}
