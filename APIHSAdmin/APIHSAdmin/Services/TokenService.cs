﻿using APIHSAdmin.DAO;
using APIHSAdmin.Model;
using APIHSAdmin.RefreshToken;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public class TokenService: ITokenService
    {
        private readonly AppSettings settings;

        public TokenService(AppSettings appSettings)
        {
            settings = appSettings;
        }

        public async Task DeleteRefreshToken(int idUsuario)
        {
            await PGData.DeleteRefreshToken(idUsuario);
        }

        public string GenerateAccessToken(IEnumerable<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(settings.SecretKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(settings.AccessTokenExpiration),
                signingCredentials: creds
            );

            string tokenString = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenString;
        }

        public string GenerateRefreshToken()
        {
            byte[] randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        //Pega as informações do token expirado
        public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            try
            {
                var tokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(settings.SecretKey)),
                    ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                SecurityToken securityToken;

                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);

                var jwtSecurityToken = securityToken as JwtSecurityToken;

                if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                    throw new SecurityTokenException("Invalid token");

                return principal;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task InsereRefreshToken(string refreshToken, int idUsuario, int expirationMinutes)
        {
            await PGData.InsereRefreshToken(refreshToken, idUsuario, expirationMinutes);
        }

        public async Task<RefreshTokenClass> SelecionaRefreshToken(int idUsuario)
        {
            RefreshTokenClass refreshToken = await PGData.SelecionaRefreshToken(idUsuario);

            return refreshToken;
        }

        public async Task UpdateRefreshToken(string refreshToken, int idUsuario, int expirationMinutes)
        {
            await PGData.UpdateRefreshToken(refreshToken, idUsuario, expirationMinutes);
        }
    }
}
