﻿using APIHSAdmin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Services
{
    public interface IUserService
    {
        Task<bool> Login(IUser user);
        string CriptoSenha(string senha);

        //DAO Functions
        Task<List<IUser>> ListaUsuario();
        Task<IUser> SelecionaUsuario(int id);
        Task InsereUsuario(IUser user);
        Task AlteraUsuario(IUser user, int id);
        Task AlteraSenhaUsuario(string senha, int id);
        Task DeletaUsuario(int id);

    }
}
