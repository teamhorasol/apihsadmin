﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Model
{
    public interface IEntidade
    {
        int IDEntidade { get; set; }
        string Nome { get; set; }
        string Cnpj { get; set; }
        string Email { get; set; }
        DateTime InicioContrato { get; set; }
        int Situacao { get; set; }
        int QuantidadeFuncionarios { get; set; }
        int QuantidadeEmpresas { get; set; }
        int QuantidadeUsuarios { get; set; }
        int IDEntidadeAcesso { get; set; }

    }
}
