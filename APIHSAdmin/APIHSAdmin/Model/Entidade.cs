﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Model
{
    public class Entidade : IEntidade
    {
        public int IDEntidade { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string Email { get; set; }
        public DateTime InicioContrato { get; set; }
        public int Situacao { get; set; }
        public int QuantidadeFuncionarios { get; set; }
        public int QuantidadeEmpresas { get; set; }
        public int QuantidadeUsuarios { get; set; }
        public int IDEntidadeAcesso { get ; set; }

        public Entidade()
        {
            IDEntidade = -1;
            Nome = "";
            Cnpj = "";
            Email = "";
            InicioContrato = new DateTime();
            Situacao = -1;
            QuantidadeEmpresas = -1;
            QuantidadeFuncionarios = -1;
            QuantidadeUsuarios = -1;
            IDEntidadeAcesso = -1;
        }

        public Entidade(JObject objetoJson)
        {
            Nome = objetoJson.Value<string>("Nome") ?? "";
            Cnpj = objetoJson.Value<string>("Cnpj").Replace(".", "").Replace("/", "").Replace("-", "") ?? "";
            Email = objetoJson.Value<string>("Email") ?? "";

            DateTime resultInicio;
            InicioContrato = DateTime.TryParse(objetoJson.Value<string>("InicioContrato"), out resultInicio) ? resultInicio : new DateTime();

            int resultSituacao = -1;
            Situacao = int.TryParse(objetoJson.Value<string>("Situacao"), out resultSituacao) ? resultSituacao : -1;

            int resultQuantidadeEmpresas = -1;
            QuantidadeEmpresas = int.TryParse(objetoJson.Value<string>("QuantidadeEmpresas"), out resultQuantidadeEmpresas) ? resultQuantidadeEmpresas : -1;

            int resultQuantidadeFuncionarios = -1;
            QuantidadeFuncionarios = int.TryParse(objetoJson.Value<string>("QuantidadeFuncionarios"), out resultQuantidadeFuncionarios) ? resultQuantidadeFuncionarios : -1;

            int resultQuantidadeUsuarios = -1;
            QuantidadeUsuarios = int.TryParse(objetoJson.Value<string>("QuantidadeUsuarios"), out resultQuantidadeUsuarios) ? resultQuantidadeUsuarios : -1;

            int resultIDEntidadeAcesso = -1;
            IDEntidadeAcesso = int.TryParse(objetoJson.Value<string>("IDEntidadeAcesso"), out resultIDEntidadeAcesso) ? resultIDEntidadeAcesso : -1;
        }
    }
}
