﻿using APIHSAdmin.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace APIHSAdmin.Model
{
    public class UsuarioAdmin : IUser
    {
        public int IDUser { get; set; }
        public string Role { get; set; }
        public string UserName { get; set; }
        public string Senha { get; set; }
        public string CPF { get; set; }
        public bool AlteraSenha { get; set; }

        public UsuarioAdmin()
        {
            IDUser = -1;
            Role = "";
            UserName = "";
            Senha = "";
            CPF = "";
        }

        public UsuarioAdmin(JObject objetoJson)
        {
            Role = objetoJson.Value<string>("Role") ?? "";
            UserName = objetoJson.Value<string>("UserName") ?? "";
            CPF = objetoJson.Value<string>("CPF") ?? "";
            CPF = CPF.Replace(".", "").Replace("-", "");

            string senhaRequest = objetoJson.Value<string>("Senha");

            AlteraSenha = objetoJson.Value<bool>("AlteraSenha");

            if (AlteraSenha)
            {
                Senha = CriptoSenha(CPF.Replace(".", "").Replace("-", ""));
            }
            else
            {
                Senha = CriptoSenha(senhaRequest);
            }

        }

        public string CriptoSenha(string senha)
        {
            if (senha != null)
            {
                if (!senha.Equals(""))
                {
                    try
                    {
                        byte[] salt = new byte[32];

                        salt = Convert.FromBase64String("31302928272625242322212019181716");

                        byte[] arrayBytesenha = new byte[32];

                        using (Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(senha, salt, 10000, HashAlgorithmName.SHA256))
                        {
                            arrayBytesenha = pbkdf2.GetBytes(32);
                        }

                        string senhaCripto = Convert.ToBase64String(arrayBytesenha);

                        return senhaCripto;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    return senha;
                }
            }
            else
            {
                return senha;
            }
        }
    }
}
