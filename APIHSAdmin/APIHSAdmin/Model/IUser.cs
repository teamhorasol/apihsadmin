﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIHSAdmin.Model
{
    public interface IUser
    {
        int IDUser { get; set; }
        string Role { get; set; }
        string UserName { get; set; }
        string Senha { get; set; }
    }
}
